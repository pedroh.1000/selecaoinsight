package insight.controllers;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import insight.models.Usuario;
import insight.models.Vaga;
import insight.repository.UsuarioRepository;

@SpringBootTest
@ContextConfiguration(classes = {UsuarioController.class})
public class VagaTest {

	private final String url = "/usuario/vaga";

	@Autowired
	private UsuarioController usuarioController;

	@MockBean
	private UsuarioRepository usuarioRepository;

	private ObjectMapper objectMapper;

	private MockMvc mvc;

	@BeforeEach
	public void setup() {
		objectMapper = new ObjectMapper();
		this.mvc = MockMvcBuilders.standaloneSetup(usuarioController).build();
	}
	
	@Test
	public void testPostAdicionarVaga() throws Exception {

		Usuario usuario = new Usuario(1, "aline", null);
		Map<String, String> values = new HashMap<>();
		values.put("idUsuario", "1");
		values.put("idVaga", "b1691d49-ee1d-4793-9c1d-7a10a9b8b84a");
		
		when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuario));

		mvc.perform(post(url+"/adicionar")
				.content(objectMapper.writeValueAsString(values))
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.id", is(1)))
		.andExpect(jsonPath("$.nome", is("aline")))
		.andExpect(jsonPath("$.listaDeInteresse[0].id", is("b1691d49-ee1d-4793-9c1d-7a10a9b8b84a")));
		
	}
	
	@Test
	public void testPostAdicionarDuasVaga() throws Exception {

		Usuario usuario = new Usuario(1, "aline", new ArrayList<Vaga>(Arrays.asList(new Vaga("b1691d49-ee1d-4793-9c1d-7a10a9b8b84a"))));
		Map<String, String> values = new HashMap<>();
		values.put("idUsuario", "1");
		values.put("idVaga", "86865d92-3b19-4e98-a441-f08b70290257");
		
		when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuario));

		mvc.perform(post(url+"/adicionar")
				.content(objectMapper.writeValueAsString(values))
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.id", is(1)))
		.andExpect(jsonPath("$.nome", is("aline")))
		.andExpect(jsonPath("$.listaDeInteresse[0].id", is("b1691d49-ee1d-4793-9c1d-7a10a9b8b84a")))
		.andExpect(jsonPath("$.listaDeInteresse[1].id", is("86865d92-3b19-4e98-a441-f08b70290257")));
		
	}
	
	@Test
	public void testPostAdicionarVagaRepetida() throws Exception {

		Usuario usuario = new Usuario(1, "aline", new ArrayList<Vaga>(Arrays.asList(new Vaga("b1691d49-ee1d-4793-9c1d-7a10a9b8b84a"))));
		Map<String, String> values = new HashMap<>();
		values.put("idUsuario", "1");
		values.put("idVaga", "b1691d49-ee1d-4793-9c1d-7a10a9b8b84a");
		
		when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuario));

		mvc.perform(post(url+"/adicionar")
				.content(objectMapper.writeValueAsString(values))
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.id", is(1)))
		.andExpect(jsonPath("$.nome", is("aline")))
		.andExpect(jsonPath("$.listaDeInteresse[0].id", is("b1691d49-ee1d-4793-9c1d-7a10a9b8b84a")))
		.andExpect(jsonPath("$.listaDeInteresse", isA(ArrayList.class)))
		.andExpect(jsonPath("$.listaDeInteresse", hasSize(1)));
	}
	
	@Test
	public void testPostRemoverVaga() throws Exception {

		Usuario usuario = new Usuario(1, "aline", new ArrayList<Vaga>(Arrays.asList(new Vaga("b1691d49-ee1d-4793-9c1d-7a10a9b8b84a"))) );
		Map<String, String> values = new HashMap<>();
		values.put("idUsuario", "1");
		values.put("idVaga", "b1691d49-ee1d-4793-9c1d-7a10a9b8b84a");
		
		when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuario));

		mvc.perform(delete(url+"/remover")
				.content(objectMapper.writeValueAsString(values))
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.id", is(1)))
		.andExpect(jsonPath("$.nome", is("aline")))
		.andExpect(jsonPath("$.listaDeInteresse",  Matchers.empty() ));
		
	}
	
	@Test
	public void testPostRemoverVagaUsuarioSemVaga() throws Exception {

		Usuario usuario = new Usuario(1, "aline", new ArrayList<Vaga>() );
		Map<String, String> values = new HashMap<>();
		values.put("idUsuario", "1");
		values.put("idVaga", "b1691d49-ee1d-4793-9c1d-7a10a9b8b84a");
		
		when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuario));
		
		mvc.perform(delete(url+"/remover")
				.content(objectMapper.writeValueAsString(values))
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.id", is(1)))
		.andExpect(jsonPath("$.nome", is("aline")))
		.andExpect(jsonPath("$.listaDeInteresse",  Matchers.empty() ));
		
	}
	
	@Test
	public void testPostRemoverBadRequest() throws Exception {

		Usuario usuario = new Usuario(1, "aline", new ArrayList<Vaga>(Arrays.asList(new Vaga("b1691d49-ee1d-4793-9c1d-7a10a9b8b84a"))) );
		Map<String, String> values = new HashMap<>();
		values.put("idUsuario", "1");
		
		when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuario));

		mvc.perform(delete(url+"/remover")
				.content(objectMapper.writeValueAsString(values))
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andExpect(status().isBadRequest());
		
	}
	
	@Test
	public void testPostAdicionarBadRequest() throws Exception {

		Usuario usuario = new Usuario(1, "aline", new ArrayList<Vaga>(Arrays.asList(new Vaga("b1691d49-ee1d-4793-9c1d-7a10a9b8b84a"))) );
		Map<String, String> values = new HashMap<>();
		values.put("idUsuario", "1");
		
		when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuario));

		mvc.perform(post(url+"/adicionar")
				.content(objectMapper.writeValueAsString(values))
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andExpect(status().isBadRequest());
		
	}
	

	@Test
	public void testPostAdicionarVagaInvalida() throws Exception {

		Usuario usuario = new Usuario(1, "aline", null);
		Map<String, String> values = new HashMap<>();
		values.put("idUsuario", "1");
		values.put("idVaga", "b1691d49-ee1d-4793-9c1d");
		
		when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuario));
		
		mvc.perform(post(url+"/adicionar")
				.content(objectMapper.writeValueAsString(values))
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andExpect(status().isBadRequest());
		
	}
}
