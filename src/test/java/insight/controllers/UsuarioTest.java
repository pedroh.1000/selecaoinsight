package insight.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import insight.models.Usuario;
import insight.repository.UsuarioRepository;

@SpringBootTest
@ContextConfiguration(classes = {UsuarioController.class})
public class UsuarioTest{

	private final String url = "/usuario";

	@Autowired
	private UsuarioController usuarioController;

	@MockBean
	private UsuarioRepository usuarioRepository;

	private ObjectMapper objectMapper;

	private MockMvc mvc;

	@BeforeEach
	public void setup() {
		objectMapper = new ObjectMapper();
		this.mvc = MockMvcBuilders.standaloneSetup(usuarioController).build();
	}

	@Test
	public void testPost() throws Exception {

		Usuario usuario = new Usuario(1, "aline", null);

		when(usuarioRepository.save(any(Usuario.class))).thenReturn(usuario);

		mvc.perform(post(url)
				.content(objectMapper.writeValueAsString(usuario))
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andExpect(status().isCreated())
		.andExpect(jsonPath("$.id", is(1)))
		.andExpect(jsonPath("$.nome", is("aline")));
	}

	@Test
	public void getOne() throws Exception{

		Usuario usuario = new Usuario(1, "aline", null);

		when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuario));

		mvc.perform(get(url + "/1").header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)).andExpect(status().isOk());
		Usuario usuarioEncontrado = usuarioRepository.findById(1).orElse(null);
		assertNotNull(usuarioEncontrado);
		assertTrue(usuarioEncontrado.getId().equals(1));
		assertTrue(usuarioEncontrado.getNome().equals("aline"));


	}

	@Test
	public void testGetAll() throws Exception {

		Usuario usuario1 = new Usuario(1, "aline", null);
		Usuario usuario2 = new Usuario(2, "alice", null);

		when(usuarioRepository.findAll()).thenReturn(Arrays.asList(usuario1, usuario2));

		mvc.perform(get(url).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)).andExpect(status().isOk());
		assertTrue(usuarioRepository.findAll().size() == 2);
	}


	@Test
	public void testDelete() throws Exception{
		Usuario usuario = new Usuario(1, "aline", null);

		when(usuarioRepository.findById(1)).thenReturn(Optional.of(usuario));

		mvc.perform(delete(url + "/1"))
		.andExpect(status().isOk());
	}

	
}
