package insight.models;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Entity
public class Usuario {

	@Id
	private Integer id;
	
	@NotBlank
	private String nome;
	
	@ElementCollection
	private List<Vaga> listaDeInteresse;
	
	public Usuario() {
	}

	public Usuario(Integer id, @NotBlank String nome, List<Vaga> listaDeInteresse) {
		this.id = id;
		this.nome = nome;
		this.listaDeInteresse = listaDeInteresse;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Vaga> getListaDeInteresse() {
		return listaDeInteresse;
	}

	public void setListaDeInteresse(List<Vaga> listaDeInteresse) {
		this.listaDeInteresse = listaDeInteresse;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", nome=" + nome + ", listaDeInteresse=" + listaDeInteresse + "]";
	}

	
}
