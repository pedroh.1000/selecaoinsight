package insight.controllers;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Callable;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import insight.models.Usuario;
import insight.models.Vaga;
import insight.repository.UsuarioRepository;
import io.swagger.annotations.Api;

@Api
@RestController
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	UsuarioRepository usuarioRepository;

	@PostMapping
	public ResponseEntity<Usuario> create(@Valid @RequestBody Usuario usuario){

		if(usuarioRepository.findById(usuario.getId()).isPresent()) {
			return ResponseEntity.status(HttpStatus.CONFLICT).build();
		}
		Usuario usuarioCriado = usuarioRepository.save(usuario);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("{id}").buildAndExpand(usuarioCriado.getId()).toUri();
		return ResponseEntity.created(uri).body(usuarioCriado);
	}

	@GetMapping("/{id}")
	public Callable<ResponseEntity<Usuario>> get(@PathVariable("id") Integer id){
		return () -> {
			Optional<Usuario> usuarioEncontrado = usuarioRepository.findById(id);
			if(usuarioEncontrado.isEmpty()) {
				return ResponseEntity.notFound().build();
			}
			List<Vaga> vagas = new ArrayList<>();
			for(Vaga vaga : usuarioEncontrado.get().getListaDeInteresse()) {
				RestTemplate rest = new RestTemplate();
				vagas.add(rest.getForObject("https://jobs.github.com/positions/" + vaga.getId(), Vaga.class));
			}

			return ResponseEntity.ok(new Usuario(id,usuarioEncontrado.get().getNome(),vagas));
		};
	}

	@GetMapping
	public Callable<ResponseEntity<List<Usuario>>> getAll(){
		return () -> {
			List<Usuario> usuarios = usuarioRepository.findAll();
			if(usuarios.isEmpty()) {
				return ResponseEntity.notFound().build();
			}

			List<Usuario> usuariosResponse = new ArrayList<>();
			for(Usuario usuario : usuarios) {
				List<Vaga> vagas = new ArrayList<>();
				for(Vaga vaga : usuario.getListaDeInteresse()) {
					RestTemplate rest = new RestTemplate();
					vagas.add(rest.getForObject("https://jobs.github.com/positions/" + vaga.getId(), Vaga.class));
				}
				usuario.setListaDeInteresse(vagas);
				usuariosResponse.add(usuario);
			}

			return ResponseEntity.ok(usuariosResponse);
		};
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Usuario> delete(@PathVariable(value = "id") Integer id)
	{
		Optional<Usuario> usuario = usuarioRepository.findById(id);
		if(usuario.isPresent()){
			usuarioRepository.delete(usuario.get());
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}

	@PostMapping("/vaga/adicionar")
	public ResponseEntity<Usuario> adicionarVaga(@RequestBody Map<String, String> values){

		if(values.get("idVaga") == null || values.get("idUsuario") == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build(); 
		}

		Optional<Usuario> usuarioEncontrado = usuarioRepository.findById(Integer.parseInt(values.get("idUsuario")));
		if(usuarioEncontrado.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		Vaga vaga = new Vaga();
		vaga.setId(values.get("idVaga"));

		try {
			RestTemplate rest = new RestTemplate();
			rest.getForObject("https://jobs.github.com/positions/" + vaga.getId(), Vaga.class);
		}catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}

		if(usuarioEncontrado.get().getListaDeInteresse() == null) {
			usuarioEncontrado.get().setListaDeInteresse(new ArrayList<>());
		}

		if(!usuarioEncontrado.get().getListaDeInteresse().contains(vaga)) {
			usuarioEncontrado.get().getListaDeInteresse().add(vaga);
		}

		usuarioRepository.saveAndFlush(usuarioEncontrado.get());

		return ResponseEntity.ok(usuarioEncontrado.get());
	}

	@DeleteMapping("/vaga/remover")
	public ResponseEntity<Usuario> removerVaga(@RequestBody Map<String, String> values){

		if(values.get("idVaga") == null || values.get("idUsuario") == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build(); 
		}

		Usuario usuarioEncontrado = usuarioRepository.findById( Integer.parseInt(values.get("idUsuario")) ).orElse(null);
		if(usuarioEncontrado == null) {
			return ResponseEntity.notFound().build();
		}

		if(usuarioEncontrado.getListaDeInteresse() == null) {
			return ResponseEntity.ok(usuarioEncontrado);
		}

		usuarioEncontrado.getListaDeInteresse().removeIf( vaga -> vaga.getId().equals(values.get("idVaga")));

		usuarioRepository.saveAndFlush(usuarioEncontrado);

		return ResponseEntity.ok(usuarioEncontrado);
	}

	
	
}
